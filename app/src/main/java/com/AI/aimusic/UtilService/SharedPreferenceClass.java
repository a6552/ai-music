package com.AI.aimusic.UtilService;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceClass {

    private static final String USER_PREFS =  "music_player";

    private SharedPreferences appSharedPreferences;
    private SharedPreferences.Editor prefsEditor;


    public SharedPreferenceClass(Context context) {
        appSharedPreferences = context.getSharedPreferences(USER_PREFS, Activity.MODE_PRIVATE);
        this.prefsEditor = appSharedPreferences.edit();
    }

    public int getValue_int(String key) {
        return appSharedPreferences.getInt(key, 0);
    }

    public void setValue_int(String key, int value) {
        prefsEditor.putInt(key, value).commit();
    }


    public String getValue_String(String key) {
        return appSharedPreferences.getString(key, "");
    }

    public void setValue_String(String key, String value) {
        prefsEditor.putString(key, value).commit();
    }


    public boolean getValue_boolean(String key) {
        return appSharedPreferences.getBoolean(key, false);
    }

    public void setValue_boolean(String key, boolean value) {
        prefsEditor.putBoolean(key, value).commit();
    }

    public void clearData() {
        prefsEditor.clear().commit();
    }
}
