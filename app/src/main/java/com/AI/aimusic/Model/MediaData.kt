package com.AI.aimusic.Model

import android.media.MediaMetadataRetriever
import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MediaData(
    var title: String?,
    var artist: String?,
    var songPath: String?,
    var album: String?
): Parcelable

fun getImageArt(path: String): ByteArray? {
    val retriever = MediaMetadataRetriever()
    retriever.setDataSource(path)
    return retriever.embeddedPicture
}




