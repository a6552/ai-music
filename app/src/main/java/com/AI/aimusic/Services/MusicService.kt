package com.AI.aimusic.Services

import android.annotation.SuppressLint
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.AudioManager
import android.media.audiofx.AcousticEchoCanceler
import android.net.Uri
import android.os.*
import android.support.v4.media.session.MediaSessionCompat
import androidx.core.app.NotificationCompat
import com.AI.aimusic.Application.ApplicationClass
import com.AI.aimusic.Model.getImageArt
import com.AI.aimusic.view.Activity.MusicPlayerActivity
import com.AI.aimusic.R
import com.AI.aimusic.Receivers.NotificationReceiver
import com.google.android.exoplayer2.ExoPlayer

import android.widget.Toast
import com.AI.aimusic.view.Activity.MusicPlayerActivity.position
import com.AI.aimusic.view.Activity.SongsListActivity
import com.AI.aimusic.view.Fagments.NowPlaying
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSource
import java.io.File
import java.io.IOException
import java.lang.Exception
import android.speech.tts.TextToSpeech
import android.speech.tts.TextToSpeech.OnInitListener
import org.vosk.Model
import org.vosk.Recognizer
import org.vosk.SpeakerModel
import org.vosk.android.RecognitionListener
import org.vosk.android.SpeechService
import org.vosk.android.SpeechStreamService
import org.vosk.android.StorageService

import java.util.*





class MusicService : Service(), AudioManager.OnAudioFocusChangeListener, RecognitionListener {

    lateinit var exoPlayer: ExoPlayer
    private val myBinder = MyBinder()
    private lateinit var mediaSession: MediaSessionCompat
    lateinit var audioManager: AudioManager
    private lateinit var notificationBuilder: NotificationCompat.Builder
    private lateinit var playPendingIntent: PendingIntent
    private lateinit var nextPendingIntent: PendingIntent
    private lateinit var prevPendingIntent: PendingIntent
    private lateinit var exitPendingIntent: PendingIntent
    private var textToSpeech: TextToSpeech? = null
    private lateinit var model: Model
    lateinit var speechService: com.AI.aimusic.Services.SpeechService


    override fun onBind(p0: Intent?): IBinder {
        mediaSession = MediaSessionCompat(baseContext, "My Music")
        return myBinder
    }

    inner class MyBinder : Binder() {
        fun currentService(): MusicService {
            return this@MusicService;
        }
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    fun showNotification(playPauseIcon: Int) {

        val intent = Intent(baseContext, SongsListActivity::class.java)
        val contentIntent = PendingIntent.getActivity(this, 0, intent, 0)

        val prevIntent = Intent(baseContext, NotificationReceiver::class.java)
            .setAction(ApplicationClass.PREVIOUS);
        prevPendingIntent = PendingIntent.getBroadcast(
            baseContext,
            0, prevIntent, PendingIntent.FLAG_UPDATE_CURRENT
        )

        val nextIntent = Intent(baseContext, NotificationReceiver::class.java)
            .setAction(ApplicationClass.NEXT);
        nextPendingIntent = PendingIntent.getBroadcast(
            baseContext,
            0, nextIntent, PendingIntent.FLAG_UPDATE_CURRENT
        )

        val playIntent = Intent(baseContext, NotificationReceiver::class.java)
            .setAction(ApplicationClass.PLAY);
        playPendingIntent = PendingIntent.getBroadcast(
            baseContext,
            0, playIntent, PendingIntent.FLAG_UPDATE_CURRENT
        )

        val exitIntent = Intent(baseContext, NotificationReceiver::class.java)
            .setAction(ApplicationClass.EXIT);
        exitPendingIntent = PendingIntent.getBroadcast(
            baseContext,
            0, exitIntent, PendingIntent.FLAG_UPDATE_CURRENT
        )

        notificationBuilder = NotificationCompat.Builder(baseContext, ApplicationClass.CHANNEL_ID)
            .setStyle(
                androidx.media.app.NotificationCompat.MediaStyle()
                    .setMediaSession(mediaSession.sessionToken)
            )
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
            .setOnlyAlertOnce(true)
            .addAction(R.drawable.ic_rewind, "Previous", prevPendingIntent)
            .addAction(playPauseIcon, "Play", playPendingIntent)
            .addAction(R.drawable.ic_fast_forward, "Next", nextPendingIntent)
            .addAction(R.drawable.ic_baseline_close_24, "Exit", exitPendingIntent)
            .setContentIntent(contentIntent)

        updateActions(playPauseIcon, "play")

    }

    fun updateActions(playPauseIcon: Int, action: String) {

        val imageArt = getImageArt(
            MusicPlayerActivity.mediaDataArrayList?.get(
                position
            )?.songPath.toString()
        )
        val image = if (imageArt != null) {
            BitmapFactory.decodeByteArray(imageArt, 0, imageArt.size)
        } else {
            BitmapFactory.decodeResource(resources, R.drawable.music1)
        }

        if (action != "playPause") {
            notificationBuilder
                .setLargeIcon(image)
                .setContentTitle(
                    MusicPlayerActivity.mediaDataArrayList?.get(
                        position
                    )?.title
                )
                .setContentText(
                    MusicPlayerActivity.mediaDataArrayList?.get(
                        position
                    )?.artist
                )
                .setSmallIcon(R.mipmap.ic_launcher)
        }

        notificationBuilder
            .clearActions()
            .addAction(R.drawable.ic_rewind, "Previous", prevPendingIntent)
            .addAction(playPauseIcon, "Play", playPendingIntent)
            .addAction(R.drawable.ic_fast_forward, "Next", nextPendingIntent)
            .addAction(R.drawable.ic_baseline_close_24, "Exit", exitPendingIntent)
        startForeground(13, notificationBuilder.build())
    }

    fun setSongPosition(increment: Boolean) {
        if (increment) {
            if (position >= (MusicPlayerActivity.mediaDataArrayList?.size)?.minus(
                    1
                )!!
            ) {
                position = 0
            } else {
                position++
            }
        } else {
            if (position == 0) {
                position =
                    (MusicPlayerActivity.mediaDataArrayList?.size)?.minus(1)!!
            } else {
                position--
            }
        }
    }

    override fun onCreate() {
        super.onCreate()
        initModel()
    }

    private fun initModel() {
        StorageService.unpack(this, "model-en-us", "model",
            { model: Model? ->
                this.model = model!!
                startRecognizer()
            }
        ) { exception: IOException ->
            Toast.makeText(this, "" +exception.message, Toast.LENGTH_SHORT).show()
        }

    }

    private fun startRecognizer() {
        try {
            textToSpeech = TextToSpeech(
                applicationContext
            ) { i ->
                if (i != TextToSpeech.ERROR) {
                    textToSpeech!!.language = Locale.ENGLISH
                }
            }

            val rec = Recognizer(model, 16000.0f,
                "[\"start\", \"stop\", \"next\", \"previous\", \"forward\", \"rewind\"]")
            speechService = com.AI.aimusic.Services.SpeechService(rec, 16000.0f)
            speechService.reset()
            speechService.startListening(this)

        } catch (e: IOException) {
            Toast.makeText(this, "" +e.message, Toast.LENGTH_SHORT).show()
        }
    }


    override fun onAudioFocusChange(p0: Int) {
        when (p0) {
            AudioManager.AUDIOFOCUS_GAIN -> {
                exoPlayer.play()
                updateActions(R.drawable.ic_pause, "playPause")
                MusicPlayerActivity.binding?.playButton?.setImageResource(R.drawable.ic_pause)
                NowPlaying.binding.playButtonNP.setImageResource(R.drawable.ic_pause)
                MusicPlayerActivity.binding?.playButton?.tag = "Pause"
                NowPlaying.binding.playButtonNP.setImageResource(R.drawable.ic_pause)
            }

            AudioManager.AUDIOFOCUS_LOSS -> {
                exoPlayer.playWhenReady = false
                updateActions(R.drawable.ic_play_white, "playPause")
                MusicPlayerActivity.binding?.playButton?.setImageResource(R.drawable.ic_play_white)
                NowPlaying.binding.playButtonNP.setImageResource(R.drawable.ic_play_white)
                MusicPlayerActivity.binding?.playButton?.tag = "Play"
                NowPlaying.binding.playButtonNP.setImageResource(R.drawable.ic_play)
            }

        }
    }

    private fun playMusic() {
        exoPlayer.playWhenReady = true
        updateActions(R.drawable.ic_pause, "playPause")
        MusicPlayerActivity.binding?.playButton?.setImageResource(R.drawable.ic_pause)
        MusicPlayerActivity.binding?.playButton?.tag = "Pause"
        NowPlaying.binding.playButtonNP.setImageResource(R.drawable.ic_pause)
    }

    private fun pauseMusic() {
        exoPlayer.playWhenReady = false
        updateActions(R.drawable.ic_play_white, "playPause")
        MusicPlayerActivity.binding?.playButton?.setImageResource(R.drawable.ic_play_white)
        MusicPlayerActivity.binding?.playButton?.tag = "Play"
        NowPlaying.binding.playButtonNP.setImageResource(R.drawable.ic_play)
    }

    private fun prevNextSong(increment: Boolean, context: Context) {
        setSongPosition(increment)


        var songFile =
            File(MusicPlayerActivity.mediaDataArrayList[position].songPath)

        val factory = DefaultDataSource.Factory(context)
        val uri = Uri.fromFile(songFile)
        val mediaItem = MediaItem.Builder().setUri(uri).build()
        val mediaSource = ProgressiveMediaSource.Factory(factory)
            .createMediaSource(mediaItem)
        exoPlayer.setMediaSource(mediaSource)
        exoPlayer.prepare()

        MusicPlayerActivity.binding.title.text =
            MusicPlayerActivity.mediaDataArrayList[position].title
        MusicPlayerActivity.binding.artist.text =
            MusicPlayerActivity.mediaDataArrayList[position].artist
        showNotification(R.drawable.ic_pause)
        Glide.with(context)
            .load(MusicPlayerActivity.mediaDataArrayList[position].album)
            .apply(RequestOptions.placeholderOf(R.drawable.music1))
            .centerCrop()
            .into(MusicPlayerActivity.binding.mainAlbum)

        NowPlaying.binding.titleNP.text =
            MusicPlayerActivity.mediaDataArrayList[position].title
        NowPlaying.binding.artistNP.text =
            MusicPlayerActivity.mediaDataArrayList[position].artist

        Glide.with(context)
            .load(MusicPlayerActivity.mediaDataArrayList[position].album)
            .apply(RequestOptions.placeholderOf(R.drawable.music1))
            .centerCrop()
            .into(NowPlaying.binding.album)

        playMusic()
    }


    override fun onDestroy() {
        super.onDestroy()
        if (speechService != null) {
            speechService.stop()
            speechService.shutdown()
        }

    }

    override fun onPartialResult(hypothesis: String?) {

    }

    override fun onResult(hypothesis: String?) {

        if (hypothesis != null) {

            var sub = hypothesis.substringAfter(':')
            var result = sub.substring(2, sub.length-3)

            if (result.isNotEmpty())
            Toast.makeText(this, ""+result, Toast.LENGTH_SHORT).show()

            if ( hypothesis.contains("start", true)
                || hypothesis.contains("restart", true) ||
                hypothesis.contains("stored", true) ||
                hypothesis.contains("restore", true)) {

                textToSpeech!!.speak("Ok Got it", TextToSpeech.QUEUE_FLUSH, null)
                Handler(Looper.myLooper()!!).postDelayed({
                    playMusic()
                }, 800)

            }
            else if(hypothesis.contains("stop", true)
                    || hypothesis.contains("is cool", true)
                || hypothesis.contains("school", true)
                || hypothesis.contains("he top", true)) {
                textToSpeech!!.speak("Ok Got it", TextToSpeech.QUEUE_FLUSH, null)
                Handler(Looper.myLooper()!!).postDelayed({
                    pauseMusic()
                }, 800)
            }

            else if(hypothesis.contains("next", true)
                || hypothesis.contains("notes", true)
                || hypothesis.contains("note", true)) {
                textToSpeech!!.speak("Ok Got it", TextToSpeech.QUEUE_FLUSH, null)
                Handler(Looper.myLooper()!!).postDelayed({
                    prevNextSong(true, this)
                }, 800)
            }

            else if(hypothesis.contains("previous", true)
                || hypothesis.contains("who is", true)) {
                textToSpeech!!.speak(
                    "Ok Got it",
                    TextToSpeech.QUEUE_FLUSH,
                    null
                )
                Handler(Looper.myLooper()!!).postDelayed({
                    prevNextSong(false, this)
                }, 800)
            }

            else if(hypothesis.contains("forward", true)
                || hypothesis.contains("howard", true)
                || hypothesis.contains("word", true)) {
                textToSpeech!!.speak(
                    "Ok Got it",
                    TextToSpeech.QUEUE_FLUSH,
                    null
                )
                Handler(Looper.myLooper()!!).postDelayed({
                    forwardRewind(true)
                }, 800)
            }

            else if(hypothesis.contains("rewind", true)
                || hypothesis.contains("right", true)
                || hypothesis.contains("world", true)
                || hypothesis.contains("wind", true)) {
                textToSpeech!!.speak("Ok Got it", TextToSpeech.QUEUE_FLUSH, null)
                Handler(Looper.myLooper()!!).postDelayed({
                    forwardRewind(false)
                }, 800)
            }
        }

    }

    private fun forwardRewind(b: Boolean) {
        if (b) {
            exoPlayer.seekTo(exoPlayer.currentPosition + 10000)
        }
        else {
            exoPlayer.seekTo(exoPlayer.currentPosition - 10000)
        }

    }

    override fun onFinalResult(hypothesis: String?) {


    }

    override fun onError(exception: Exception?) {

    }

    override fun onTimeout() {
        speechService.startListening(this)
    }
}