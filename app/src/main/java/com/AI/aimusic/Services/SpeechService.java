package com.AI.aimusic.Services;


import android.annotation.SuppressLint;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.media.audiofx.AcousticEchoCanceler;
import android.os.Handler;
import android.os.Looper;
import org.vosk.Recognizer;
import org.vosk.android.RecognitionListener;

import java.io.IOException;

public class SpeechService {
    private final Recognizer recognizer;
    private final int sampleRate;
    private static final float BUFFER_SIZE_SECONDS = 0.2F;
    private final int bufferSize;
    public static AudioRecord recorder;
    private RecognizerThread recognizerThread;
    private final Handler mainHandler = new Handler(Looper.getMainLooper());

    @SuppressLint("MissingPermission")
    public SpeechService(Recognizer recognizer, float sampleRate) throws IOException {
        this.recognizer = recognizer;
        this.sampleRate = (int)sampleRate;
        this.bufferSize = Math.round((float)this.sampleRate * 0.2F);
        recorder = new AudioRecord(MediaRecorder.AudioSource.VOICE_COMMUNICATION, this.sampleRate, 16, 2, bufferSize * 2);
        boolean isAvailable = AcousticEchoCanceler.isAvailable();
        if (isAvailable) {
            AcousticEchoCanceler acousticEchoCanceler = AcousticEchoCanceler.create(recorder.getAudioSessionId());
            if (acousticEchoCanceler != null) {
                acousticEchoCanceler.setEnabled(true);
            }
        }

        if (recorder.getState() == 0) {
            recorder.release();
            throw new IOException("Failed to initialize recorder. Microphone might be already in use.");
        }
    }

    public boolean startListening(RecognitionListener listener) {
        if (null != this.recognizerThread) {
            return false;
        } else {
            this.recognizerThread = new RecognizerThread(listener);
            this.recognizerThread.start();
            return true;
        }
    }

    public boolean startListening(RecognitionListener listener, int timeout) {
        if (null != this.recognizerThread) {
            return false;
        } else {
            this.recognizerThread = new RecognizerThread(listener, timeout);
            this.recognizerThread.start();
            return true;
        }
    }

    private boolean stopRecognizerThread() {
        if (null == this.recognizerThread) {
            return false;
        } else {
            try {
                this.recognizerThread.interrupt();
                this.recognizerThread.join();
            } catch (InterruptedException var2) {
                Thread.currentThread().interrupt();
            }

            this.recognizerThread = null;
            return true;
        }
    }

    public boolean stop() {
        return this.stopRecognizerThread();
    }

    public boolean cancel() {
        if (this.recognizerThread != null) {
            this.recognizerThread.setPause(true);
        }

        return this.stopRecognizerThread();
    }

    public void shutdown() {
        this.recorder.release();
    }

    public void setPause(boolean paused) {
        if (this.recognizerThread != null) {
            this.recognizerThread.setPause(paused);
        }

    }

    public void reset() {
        if (this.recognizerThread != null) {
            this.recognizerThread.reset();
        }

    }

    private final class RecognizerThread extends Thread {
        private int remainingSamples;
        private final int timeoutSamples;
        private static final int NO_TIMEOUT = -1;
        private volatile boolean paused;
        private volatile boolean reset;
        RecognitionListener listener;

        public RecognizerThread(RecognitionListener listener, int timeout) {
            this.paused = false;
            this.reset = false;
            this.listener = listener;
            if (timeout != -1) {
                this.timeoutSamples = timeout * sampleRate / 1000;
            } else {
                this.timeoutSamples = -1;
            }

            this.remainingSamples = this.timeoutSamples;
        }

        public RecognizerThread(RecognitionListener listener) {
            this(listener, -1);
        }

        public void setPause(boolean paused) {
            this.paused = paused;
        }

        public void reset() {
            this.reset = true;
        }

        public void run() {
           recorder.startRecording();
            if (recorder.getRecordingState() == 1) {
                recorder.stop();
                IOException ioe = new IOException("Failed to start recording. Microphone might be already in use.");
                mainHandler.post(() -> {
                    this.listener.onError(ioe);
                });
            }

            short[] buffer = new short[bufferSize];

            while(!interrupted() && (this.timeoutSamples == -1 || this.remainingSamples > 0)) {
                int nread = recorder.read(buffer, 0, buffer.length);
                if (!this.paused) {
                    if (this.reset) {
                       recognizer.reset();
                        this.reset = false;
                    }

                    if (nread < 0) {
                        throw new RuntimeException("error reading audio buffer");
                    }

                    String result;
                    if (recognizer.acceptWaveForm(buffer, nread)) {
                        result = recognizer.getResult();
                        mainHandler.post(() -> {
                            this.listener.onResult(result);
                        });
                    } else {
                        result = recognizer.getPartialResult();
                        mainHandler.post(() -> {
                            this.listener.onPartialResult(result);
                        });
                    }

                    if (this.timeoutSamples != -1) {
                        this.remainingSamples -= nread;
                    }
                }
            }

           recorder.stop();
            if (!this.paused) {
                if (this.timeoutSamples != -1 && this.remainingSamples <= 0) {
                    mainHandler.post(() -> {
                        this.listener.onTimeout();
                    });
                } else {
                    String finalResult = recognizer.getFinalResult();
                    mainHandler.post(() -> {
                        this.listener.onFinalResult(finalResult);
                    });
                }
            }

        }
    }
}
