package com.AI.aimusic.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "PlaylistSongs", indices = [Index(value = ["songPath", "playlistId"], unique = true)])
data class PlaylistSongEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int = 0,
    @ColumnInfo(name = "title")
    val title: String,
    @ColumnInfo(name = "artist")
    val artist: String,
    @ColumnInfo(name = "songPath")
    val songPath: String,
    @ColumnInfo(name = "album")
    val album: String,
    @ColumnInfo(name = "playlistId")
    val playlistId: Int,
)