package com.AI.aimusic.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Playlists")
data class PlaylistEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int = 0,
    @ColumnInfo(name = "playlistName")
    val playlistName: String,
    @ColumnInfo(name = "playlistImage")
    val playlistImage: Int,
    @ColumnInfo(name = "createdAt")
    val createdAt: String,
)