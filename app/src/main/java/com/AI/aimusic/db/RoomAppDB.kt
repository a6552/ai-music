package com.AI.aimusic.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [SongEntity::class, PlaylistEntity::class, PlaylistSongEntity::class], version = 1)
abstract class RoomAppDB: RoomDatabase() {
    abstract fun songsDao(): SongsDao?

    companion object {
        private var INSTANCE: RoomAppDB?=null
        fun getAppDatabase(context: Context): RoomAppDB? {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder<RoomAppDB>(
                        context.applicationContext, RoomAppDB::class.java, "FavouriteSongs"
                ).allowMainThreadQueries().build();
            }

            return INSTANCE;
        }
    }

}