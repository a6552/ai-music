package com.AI.aimusic.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.AI.aimusic.db.PlaylistEntity
import com.AI.aimusic.db.PlaylistSongEntity
import com.AI.aimusic.db.SongEntity

@Dao
interface SongsDao {

    @Query("SELECT * From FavouriteSongsList ORDER BY id DESC")
    fun getAllFavouriteSongs(): LiveData<List<SongEntity>>?

    @Query("SELECT * From Playlists ORDER BY id DESC")
    fun getAllPlaylists(): LiveData<List<PlaylistEntity>>?

    @Query("SELECT * From PlaylistSongs where playlistId=:playlistId ORDER BY id DESC")
    fun getAllPlaylistSongs(playlistId: Int): LiveData<List<PlaylistSongEntity>>?

    @Insert
    fun insertFavouriteSongs(songEntity: SongEntity)

    @Insert
    fun createPlaylist(playlistEntity: PlaylistEntity)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertSongIntoPlaylist(playlistSongEntity: PlaylistSongEntity)

    @Query("SELECT EXISTS(SELECT * FROM FavouriteSongsList where songPath=:songPath)")
    fun hasItem(songPath: String): Boolean

    @Query("DELETE FROM FavouriteSongsList where songPath=:songPath")
    fun deleteFrom(songPath: String)

    @Query("DELETE FROM PlaylistSongs where playlistId=:playlistId")
    fun deleteFromPlaylist(playlistId: Int)

    @Query("DELETE FROM PlaylistSongs where playlistId=:playlistId AND songPath=:songPath")
    fun deleteSingleSongFromPlaylist(songPath: String, playlistId: Int)

    @Query("DELETE FROM Playlists where id=:playlistId")
    fun deletePlaylist(playlistId: Int)

}