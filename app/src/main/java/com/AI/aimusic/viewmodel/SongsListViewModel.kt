package com.AI.aimusic.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.AI.aimusic.Model.MediaData
import com.AI.aimusic.repository.SongsRepository

class SongsListViewModel: ViewModel() {

    private var songList:MutableLiveData<List<MediaData>>
    private var songsRepository: SongsRepository

    init {
        songList = MutableLiveData()
        songsRepository = SongsRepository()
    }

    fun getAllSongs(context:Context): MutableLiveData<List<MediaData>> {
        if (songsRepository == null) {
            songsRepository = SongsRepository()
        }

        songList = songsRepository.getSongsList(context)

        return songList
    }

}