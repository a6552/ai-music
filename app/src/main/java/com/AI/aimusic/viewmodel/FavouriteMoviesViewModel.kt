package com.AI.aimusic.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.AI.aimusic.db.PlaylistEntity
import com.AI.aimusic.db.PlaylistSongEntity
import com.AI.aimusic.db.RoomAppDB
import com.AI.aimusic.db.SongEntity
import com.AI.aimusic.db.SongsDao

class FavouriteMoviesViewModel(app: Application): AndroidViewModel(app) {

     private var allMovies: LiveData<List<SongEntity>>
     private var allPlaylist:LiveData<List<PlaylistEntity>>
    private var playlistSongs:LiveData<List<PlaylistSongEntity>>
    private var isAdded: Boolean
    private var moviesDao: SongsDao

    init {
        playlistSongs = MutableLiveData()
        allPlaylist = MutableLiveData()
        allMovies = MutableLiveData();
        isAdded = true;
        moviesDao = RoomAppDB.getAppDatabase((getApplication()))?.songsDao()!!
    }

    fun getAllSongsInFavourite(): LiveData<List<SongEntity>> {
        allMovies = moviesDao.getAllFavouriteSongs()!!
        return allMovies
    }

    fun getAllPlaylists(): LiveData<List<PlaylistEntity>> {
        allPlaylist = moviesDao.getAllPlaylists()!!
        return allPlaylist
    }

    fun getAllPlaylistSongs(playlistId: Int): LiveData<List<PlaylistSongEntity>> {
        playlistSongs = moviesDao.getAllPlaylistSongs(playlistId)!!
        return playlistSongs
    }

    fun isAddedToFavourite(url: String): Boolean {
        isAdded = (moviesDao.hasItem(url))
        return isAdded;
    }



    fun deleteFromFavourite(url: String) {
        moviesDao.deleteFrom(url)

    }

    fun deleteSongsFromPlaylist(playlistId: Int) {
        moviesDao.deleteFromPlaylist(playlistId)
    }


    fun deletePlaylist(playlistId: Int) {
        moviesDao.deletePlaylist(playlistId)
    }

    fun deleteSingleSongsFromPlaylist(songPath: String, playlistId: Int) {
        moviesDao.deleteSingleSongFromPlaylist(songPath, playlistId)
    }

    fun insertFavouriteMovies(songEntity: SongEntity){
      val movieDao =  RoomAppDB.getAppDatabase(getApplication())?.songsDao()
        movieDao?.insertFavouriteSongs(songEntity);
    }

    fun createPlaylist(playlistEntity: PlaylistEntity){
        val movieDao =  RoomAppDB.getAppDatabase(getApplication())?.songsDao()
        movieDao?.createPlaylist(playlistEntity);
    }

    fun insertSongToPlaylist(playlistSongEntity: PlaylistSongEntity){
        val movieDao =  RoomAppDB.getAppDatabase(getApplication())?.songsDao()
        movieDao?.insertSongIntoPlaylist(playlistSongEntity);
    }
}