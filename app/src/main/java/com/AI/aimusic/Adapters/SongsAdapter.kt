package com.AI.aimusic.Adapters

import android.content.Context
import com.AI.aimusic.Model.MediaData
import com.AI.aimusic.Adapters.SongsAdapter.SongsViewHolder
import android.view.ViewGroup
import android.view.LayoutInflater
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import androidx.recyclerview.widget.RecyclerView
import com.AI.aimusic.view.Activity.MusicPlayerActivity
import com.AI.aimusic.databinding.SingleSongBinding
import com.AI.aimusic.Interfaces.RecyclerVIewClickListener
import java.util.*
import android.view.MenuItem
import android.widget.PopupMenu
import com.AI.aimusic.R

class SongsAdapter(
    private var mediaDataArrayList: ArrayList<MediaData>,
    private val mContext: Context,
    private var from: String,
    private var recyclerVIewClickListener: RecyclerVIewClickListener
) : RecyclerView.Adapter<SongsViewHolder>() {

    private var secondList:ArrayList<MediaData> = mediaDataArrayList
    private val recyclerViewClickListener: RecyclerVIewClickListener = recyclerVIewClickListener


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongsViewHolder {
        val binding = SingleSongBinding.inflate(LayoutInflater.from(parent.context))
        return SongsViewHolder(binding, mContext)
    }

    override fun onBindViewHolder(holder: SongsViewHolder, position: Int) {
        val mediaData = mediaDataArrayList[position]
        holder.bind(mediaData, from)
        val index:Int = secondList.indexOf(mediaData)

        holder.binding.root.setOnClickListener {
            if (MusicPlayerActivity.musicService != null) {
                if (index == MusicPlayerActivity.position &&
                    mediaData.title == MusicPlayerActivity.mediaDataArrayList[MusicPlayerActivity.position].title) {
                    openNewActivity("nowPlaying", index)
                }
                else {
                    openNewActivity(from, index)
                }
            }
            else {
                openNewActivity(from, index)
            }

        }

        holder.binding.play.setOnClickListener {
            if (holder.binding.playIcon.tag == "dot_icon") {
                val popupMenu = PopupMenu(mContext, holder.binding.play)
                popupMenu.inflate(com.AI.aimusic.R.menu.pop_up_menu)
                popupMenu.setOnMenuItemClickListener(object : MenuItem.OnMenuItemClickListener,
                    PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(p0: MenuItem?): Boolean {
                        recyclerViewClickListener.onDeleteClick(index)
                        return true
                    }

                })
                popupMenu.show()
            }

        }

    }

    private fun openNewActivity(from: String, position: Int) {
       recyclerViewClickListener.onItemClick(position, from)
    }

    override fun getItemCount(): Int {
        return mediaDataArrayList.size
    }

    fun updateMusicList(filterList: ArrayList<MediaData>) {
        mediaDataArrayList = ArrayList()
        mediaDataArrayList.clear()
        mediaDataArrayList.addAll(filterList)
        notifyDataSetChanged()
    }

    class SongsViewHolder(var binding: SingleSongBinding, mContext: Context) : RecyclerView.ViewHolder(
        binding.root
    ) {
        private val context = mContext

        fun bind(item: MediaData, from:String) {  //<--bind method allows the ViewHolder to bind to the data it is displaying
            binding.title.text = item.title
            binding.artist.text = item.artist
            Glide.with(context)
                .load(item.album)
                .apply(RequestOptions.placeholderOf(R.drawable.music1))
                .centerCrop()
                .into(binding.musicIcon)

            when (from) {
                context.getString(R.string.from_playlist) -> {
                    binding.playIcon.setImageResource(R.drawable.ic_dots)
                    binding.playIcon.setColorFilter(context.resources.getColor(R.color.icon_color))
                    binding.playIcon.tag = "dot_icon"
                }
                context.getString(R.string.from_songsList) -> {
                    binding.playIcon.setImageResource(R.drawable.ic_play)
                    binding.playIcon.tag = "play_icon"
                }
            }

        }

    }

}