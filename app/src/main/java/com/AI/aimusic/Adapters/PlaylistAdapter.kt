package com.AI.aimusic.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.AI.aimusic.databinding.PlaylistViewBinding
import com.AI.aimusic.db.PlaylistEntity
import com.AI.aimusic.view.Activity.PlaylistDetailsActivity
import android.R
import android.widget.Toast
import com.bumptech.glide.Glide
import java.util.*
import kotlin.collections.ArrayList


class PlaylistAdapter(
private val context: Context,
private var playlistList: ArrayList<PlaylistEntity>
): RecyclerView.Adapter<PlaylistAdapter.PlaylistViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaylistViewHolder {
        val binding = PlaylistViewBinding.inflate(LayoutInflater.from(parent.context))
        return PlaylistViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PlaylistViewHolder, position: Int) {
        var mediaData = playlistList[position]
        holder.binding.playlistName.text = mediaData.playlistName
        holder.binding.date.text = mediaData.createdAt
        holder.binding.playlistImage.setImageResource(mediaData.playlistImage)

        holder.binding.root.setOnClickListener {
            val intent = Intent(context, PlaylistDetailsActivity::class.java)
            intent.putExtra("playlistId", mediaData.id)
            intent.putExtra("playlistName", mediaData.playlistName)
            intent.putExtra("image", mediaData.playlistImage)
            context.startActivity(intent)
        }

    }

    override fun getItemCount(): Int {
       return playlistList.size
    }

    class PlaylistViewHolder(var binding: PlaylistViewBinding)
        : RecyclerView.ViewHolder(binding.root)
}