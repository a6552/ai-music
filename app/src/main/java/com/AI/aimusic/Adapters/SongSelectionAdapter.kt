package com.AI.aimusic.Adapters

import android.content.Context
import com.AI.aimusic.Model.MediaData
import android.view.ViewGroup
import android.view.LayoutInflater
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.AI.aimusic.R
import androidx.recyclerview.widget.RecyclerView
import com.AI.aimusic.databinding.SingleSongBinding
import com.AI.aimusic.Interfaces.RecyclerVIewClickListener
import com.AI.aimusic.Interfaces.SongSelectionRecyclerVIewClickListener
import com.AI.aimusic.databinding.SongSelectionBinding
import com.AI.aimusic.viewmodel.FavouriteMoviesViewModel
import java.util.*


class SongSelectionAdapter(
    private var mediaDataArrayList: ArrayList<MediaData>,
    private val mContext: Context,
    private val recyclerVIewClickListener: SongSelectionRecyclerVIewClickListener,
) : RecyclerView.Adapter<SongSelectionAdapter.SongsSelectionViewHolder>() {

    private var secondList:ArrayList<MediaData> = mediaDataArrayList
    private var songSelectionRecyclerVIewClickListener = recyclerVIewClickListener


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SongSelectionAdapter.SongsSelectionViewHolder {
        val binding = SongSelectionBinding.inflate(LayoutInflater.from(parent.context))
        return SongsSelectionViewHolder(binding, mContext)
    }

    override fun onBindViewHolder(holder: SongsSelectionViewHolder, position: Int) {
        val mediaData = mediaDataArrayList[position]
        holder.bind(mediaData)
        val index:Int = secondList.indexOf(mediaData)

        holder.binding.root.setOnClickListener {
            songSelectionRecyclerVIewClickListener.onSongSelectionItemClick(index, holder.binding)
        }
    }

    override fun getItemCount(): Int {
        return mediaDataArrayList.size
    }

    fun updateMusicList(filterList: ArrayList<MediaData>) {
        mediaDataArrayList = ArrayList()
        mediaDataArrayList.clear()
        mediaDataArrayList.addAll(filterList)
        notifyDataSetChanged()
    }

    class SongsSelectionViewHolder(var binding: SongSelectionBinding, mContext: Context) : RecyclerView.ViewHolder(
        binding.root
    ) {
        private val context = mContext

        fun bind(item: MediaData) {  //<--bind method allows the ViewHolder to bind to the data it is displaying
            binding.title.text = item.title
            binding.artist.text = item.artist
            Glide.with(context)
                .load(item.album)
                .apply(RequestOptions.placeholderOf(R.drawable.music1))
                .centerCrop()
                .into(binding.musicIcon)

            binding.playIcon.setImageResource(R.drawable.ic_add)

        }

    }


}