package com.AI.aimusic.repository

import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import androidx.lifecycle.MutableLiveData
import com.AI.aimusic.Model.MediaData
import com.AI.aimusic.view.Activity.SongsListActivity

class SongsRepository {

    public fun getSongsList(context: Context): MutableLiveData<List<MediaData>> {
        var songsListLiveData = MutableLiveData<List<MediaData>>()
        var songsList = ArrayList<MediaData>()
        val contentResolver = context.contentResolver
        val songUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        val songCursor = contentResolver.query(
            songUri, null, null, null, MediaStore.Audio.Media.DATE_ADDED + " DESC"
        )
        if (songCursor != null && songCursor.moveToFirst()) {
            val songTitle = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE)
            val artist = songCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST)
            val songsUri = songCursor.getColumnIndex(MediaStore.Audio.Media.DATA)
            val album = songCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID)
            do {
                val currentTitle = songCursor.getString(songTitle)
                val currentArtist = songCursor.getString(artist)
                val data = songCursor.getString(songsUri)
                val currentAlbum = songCursor.getString(album)
                val albumUri = Uri.withAppendedPath(
                    Uri.parse("content://media/external/audio/albumart"), currentAlbum
                ).toString()
                val mediaData = MediaData(currentTitle, currentArtist, data, albumUri)
                songsList.add(mediaData)
            } while (songCursor.moveToNext())
        }

        songsListLiveData.value = songsList

        return songsListLiveData

    }



}