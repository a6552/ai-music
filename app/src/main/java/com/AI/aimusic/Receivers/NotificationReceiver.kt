package com.AI.aimusic.Receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import com.AI.aimusic.Application.ApplicationClass
import com.AI.aimusic.view.Activity.MusicPlayerActivity
import com.AI.aimusic.R
import com.AI.aimusic.view.Activity.SongsListActivity
import com.AI.aimusic.view.Fagments.NowPlaying
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSource
import java.io.File
import kotlin.system.exitProcess

class NotificationReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, p1: Intent?) {
      when(p1?.action) {
          ApplicationClass.PREVIOUS ->
              prevNextSong(false, context!!)
          ApplicationClass.PLAY -> {
              if (MusicPlayerActivity.musicService?.exoPlayer?.isPlaying == true) {
                  pauseMusic()
              } else {
                  playMusic()
              }
          }
          ApplicationClass.NEXT ->
              prevNextSong(true, context!!)
          ApplicationClass.EXIT ->{
                  MusicPlayerActivity.musicService.exoPlayer.release()
                  MusicPlayerActivity.musicService!!.stopForeground(true)
                  MusicPlayerActivity.musicService.stopSelf()
                  MusicPlayerActivity.musicService.audioManager.abandonAudioFocus(MusicPlayerActivity.musicService)
              if (MusicPlayerActivity.musicService.speechService != null) {
                  MusicPlayerActivity.musicService.speechService.stop()
                  MusicPlayerActivity.musicService.speechService.shutdown()
              }
                  NowPlaying.removeNowPlaying()
                  MusicPlayerActivity.musicService = null

          }
      }
    }

    private fun playMusic() {
        MusicPlayerActivity.musicService!!.exoPlayer.playWhenReady = true
        MusicPlayerActivity.musicService!!.updateActions(R.drawable.ic_pause, "playPause")
        MusicPlayerActivity.binding?.playButton?.setImageResource(R.drawable.ic_pause)
        MusicPlayerActivity.binding?.playButton?.tag  = "Pause"
        NowPlaying.binding.playButtonNP.setImageResource(R.drawable.ic_pause)
    }

    private fun pauseMusic() {
        MusicPlayerActivity.musicService!!.exoPlayer.playWhenReady = false
        MusicPlayerActivity.musicService!!.updateActions(R.drawable.ic_play_white, "playPause")
        MusicPlayerActivity.binding?.playButton?.setImageResource(R.drawable.ic_play_white)
        MusicPlayerActivity.binding?.playButton?.tag = "Play"
        NowPlaying.binding.playButtonNP.setImageResource(R.drawable.ic_play)
    }

    private fun prevNextSong(increment: Boolean, context: Context) {
        MusicPlayerActivity.musicService.setSongPosition(increment)
        var songFile =
            File(MusicPlayerActivity.mediaDataArrayList[MusicPlayerActivity.position].songPath)

        val factory = DefaultDataSource.Factory(context)
        val uri = Uri.fromFile(songFile)
        val mediaItem = MediaItem.Builder().setUri(uri).build()
        val mediaSource = ProgressiveMediaSource.Factory(factory)
            .createMediaSource(mediaItem)
        MusicPlayerActivity.musicService.exoPlayer.setMediaSource(mediaSource)
        MusicPlayerActivity.musicService.exoPlayer.prepare()

        MusicPlayerActivity.binding.title.text =
            MusicPlayerActivity.mediaDataArrayList[MusicPlayerActivity.position].title
        MusicPlayerActivity.binding.artist.text =
            MusicPlayerActivity.mediaDataArrayList[MusicPlayerActivity.position].artist
        MusicPlayerActivity.musicService.updateActions(R.drawable.ic_pause, "next")
        Glide.with(context)
            .load(MusicPlayerActivity.mediaDataArrayList[MusicPlayerActivity.position].album)
            .apply(RequestOptions.placeholderOf(R.drawable.music1))
            .centerCrop()
            .into(MusicPlayerActivity.binding.mainAlbum)

        NowPlaying.binding.titleNP.text =
            MusicPlayerActivity.mediaDataArrayList[MusicPlayerActivity.position].title
        NowPlaying.binding.artistNP.text =
            MusicPlayerActivity.mediaDataArrayList[MusicPlayerActivity.position].artist

        Glide.with(context)
            .load(MusicPlayerActivity.mediaDataArrayList[MusicPlayerActivity.position].album)
            .apply(RequestOptions.placeholderOf(R.drawable.music1))
            .centerCrop()
            .into(NowPlaying.binding.album)

        playMusic()
    }
}