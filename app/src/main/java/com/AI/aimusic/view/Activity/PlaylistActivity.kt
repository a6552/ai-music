package com.AI.aimusic.view.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.AI.aimusic.Adapters.PlaylistAdapter
import com.AI.aimusic.Adapters.SongsAdapter
import com.AI.aimusic.Model.MediaData
import com.AI.aimusic.R
import com.AI.aimusic.databinding.ActivityPlaylistBinding
import com.AI.aimusic.databinding.AddPlaylistDialogBinding
import com.AI.aimusic.db.PlaylistEntity
import com.AI.aimusic.viewmodel.FavouriteMoviesViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class PlaylistActivity : AppCompatActivity(), View.OnClickListener {

    private var binding:ActivityPlaylistBinding?= null
    private var playlistAdapter:PlaylistAdapter?= null
    private var playlistList = ArrayList<PlaylistEntity>()
    private lateinit var favouriteMoviesViewModel: FavouriteMoviesViewModel
    private var binder: AddPlaylistDialogBinding ?= null
    private var selectedImage:Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityPlaylistBinding.inflate(layoutInflater)
        setContentView(binding!!.root)

        favouriteMoviesViewModel = ViewModelProvider(this).get(FavouriteMoviesViewModel::class.java)
        initData()
    }

    private fun initData() {
        binding!!.playlistRecyclerview.layoutManager =
            LinearLayoutManager(this)
        binding!!.playlistRecyclerview.isNestedScrollingEnabled = false
        binding!!.playlistRecyclerview.setHasFixedSize(true)
        binding!!.playlistRecyclerview.itemAnimator = DefaultItemAnimator()

        binding!!.newPlaylist.setOnClickListener(this)

        getPlaylists()


    }

    private fun getPlaylists() {
        playlistList.clear()
        favouriteMoviesViewModel.getAllPlaylists().observe(this, {
            if (it != null) {
                playlistList = it as ArrayList<PlaylistEntity>
                playlistAdapter = PlaylistAdapter(this, playlistList)
                binding!!.playlistRecyclerview.adapter = playlistAdapter
                playlistAdapter!!.notifyDataSetChanged()
            }
            else {
                Toast.makeText(this, "Null", Toast.LENGTH_SHORT).show()

            }
        })
    }

    private fun createPlaylistDialog() {
        val customDialog = LayoutInflater.from(this)
            .inflate(R.layout.add_playlist_dialog, binding!!.root, false)
        binder = AddPlaylistDialogBinding.bind(customDialog)

        binder!!.playlistImage1.setOnClickListener(this)
        binder!!.playlistImage2.setOnClickListener(this)
        binder!!.playlistImage3.setOnClickListener(this)
        binder!!.playlistImage4.setOnClickListener(this)
        binder!!.playlistImage5.setOnClickListener(this)
        binder!!.playlistImage6.setOnClickListener(this)
        binder!!.playlistImage7.setOnClickListener(this)
        binder!!.playlistImage8.setOnClickListener(this)
        binder!!.playlistImage9.setOnClickListener(this)

        val builder = MaterialAlertDialogBuilder(this)
        builder.setView(customDialog)
            .setPositiveButton("Create") {dialog, _ ->
                var playlistName = binder!!.playlistName.text.toString()
                if (TextUtils.isEmpty(playlistName)) {
                    Toast.makeText(this, "Please enter the name..", Toast.LENGTH_SHORT).show()
                }
                else if (selectedImage == 0) {
                    Toast.makeText(this, "Please select an image..", Toast.LENGTH_SHORT).show()

                }
                else {
                    addPlaylist(playlistName)
                    dialog.dismiss()
                }

            }

        builder.show()
    }

    private fun addPlaylist(playlistName: String) {
        var calendar = Calendar.getInstance().time
        val sdf = SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH)
        var playlistEntity = PlaylistEntity(0, playlistName, selectedImage, sdf.format(calendar))
        favouriteMoviesViewModel.createPlaylist(playlistEntity)
        Toast.makeText(this, "Created", Toast.LENGTH_SHORT).show()
    }

    override fun onClick(p0: View?) {
        when(p0!!.id) {
            R.id.new_playlist -> {
                createPlaylistDialog()
            }
            R.id.playlist_image1 -> {
                binder!!.playlistImage1.strokeWidth = 10f
                binder!!.playlistImage2.strokeWidth = 0f
                binder!!.playlistImage3.strokeWidth = 0f
                binder!!.playlistImage4.strokeWidth = 0f
                binder!!.playlistImage5.strokeWidth = 0f
                binder!!.playlistImage6.strokeWidth = 0f
                binder!!.playlistImage7.strokeWidth = 0f
                binder!!.playlistImage8.strokeWidth = 0f
                binder!!.playlistImage9.strokeWidth = 0f
                selectedImage = R.drawable.party
            }
            R.id.playlist_image2 -> {
                binder!!.playlistImage1.strokeWidth = 0f
                binder!!.playlistImage2.strokeWidth = 10f
                binder!!.playlistImage3.strokeWidth = 0f
                binder!!.playlistImage4.strokeWidth = 0f
                binder!!.playlistImage5.strokeWidth = 0f
                binder!!.playlistImage6.strokeWidth = 0f
                binder!!.playlistImage7.strokeWidth = 0f
                binder!!.playlistImage8.strokeWidth = 0f
                binder!!.playlistImage9.strokeWidth = 0f
                selectedImage = R.drawable.travel
            }
            R.id.playlist_image3 -> {
                binder!!.playlistImage1.strokeWidth = 0f
                binder!!.playlistImage2.strokeWidth = 0f
                binder!!.playlistImage3.strokeWidth = 10f
                binder!!.playlistImage4.strokeWidth = 0f
                binder!!.playlistImage5.strokeWidth = 0f
                binder!!.playlistImage6.strokeWidth = 0f
                binder!!.playlistImage7.strokeWidth = 0f
                binder!!.playlistImage8.strokeWidth = 0f
                binder!!.playlistImage9.strokeWidth = 0f
                selectedImage = R.drawable.excercise
            }
            R.id.playlist_image4 -> {
                binder!!.playlistImage1.strokeWidth = 0f
                binder!!.playlistImage2.strokeWidth = 0f
                binder!!.playlistImage3.strokeWidth = 0f
                binder!!.playlistImage4.strokeWidth = 10f
                binder!!.playlistImage5.strokeWidth = 0f
                binder!!.playlistImage6.strokeWidth = 0f
                binder!!.playlistImage7.strokeWidth = 0f
                binder!!.playlistImage8.strokeWidth = 0f
                binder!!.playlistImage9.strokeWidth = 0f
                selectedImage = R.drawable.sleep
            }
            R.id.playlist_image5 -> {
                binder!!.playlistImage1.strokeWidth = 0f
                binder!!.playlistImage2.strokeWidth = 0f
                binder!!.playlistImage3.strokeWidth = 0f
                binder!!.playlistImage4.strokeWidth = 0f
                binder!!.playlistImage5.strokeWidth = 10f
                binder!!.playlistImage6.strokeWidth = 0f
                binder!!.playlistImage7.strokeWidth = 0f
                binder!!.playlistImage8.strokeWidth = 0f
                binder!!.playlistImage9.strokeWidth = 0f
                selectedImage = R.drawable.driving
            }
            R.id.playlist_image6  -> {
                binder!!.playlistImage1.strokeWidth = 0f
                binder!!.playlistImage2.strokeWidth = 0f
                binder!!.playlistImage3.strokeWidth = 0f
                binder!!.playlistImage4.strokeWidth = 0f
                binder!!.playlistImage5.strokeWidth = 0f
                binder!!.playlistImage6.strokeWidth = 10f
                binder!!.playlistImage7.strokeWidth = 0f
                binder!!.playlistImage8.strokeWidth = 0f
                binder!!.playlistImage9.strokeWidth = 0f
                selectedImage = R.drawable.peace
            }
            R.id.playlist_image7  -> {
                binder!!.playlistImage1.strokeWidth = 0f
                binder!!.playlistImage2.strokeWidth = 0f
                binder!!.playlistImage3.strokeWidth = 0f
                binder!!.playlistImage4.strokeWidth = 0f
                binder!!.playlistImage5.strokeWidth = 0f
                binder!!.playlistImage6.strokeWidth = 0f
                binder!!.playlistImage7.strokeWidth = 10f
                binder!!.playlistImage8.strokeWidth = 0f
                binder!!.playlistImage9.strokeWidth = 0f
                selectedImage = R.drawable.background7
            }
            R.id.playlist_image8  -> {
                binder!!.playlistImage1.strokeWidth = 0f
                binder!!.playlistImage2.strokeWidth = 0f
                binder!!.playlistImage3.strokeWidth = 0f
                binder!!.playlistImage4.strokeWidth = 0f
                binder!!.playlistImage5.strokeWidth = 0f
                binder!!.playlistImage6.strokeWidth = 0f
                binder!!.playlistImage7.strokeWidth = 0f
                binder!!.playlistImage8.strokeWidth = 10f
                binder!!.playlistImage9.strokeWidth = 0f
                selectedImage = R.drawable.background8
            }
            R.id.playlist_image9  -> {
                binder!!.playlistImage1.strokeWidth = 0f
                binder!!.playlistImage2.strokeWidth = 0f
                binder!!.playlistImage3.strokeWidth = 0f
                binder!!.playlistImage4.strokeWidth = 0f
                binder!!.playlistImage5.strokeWidth = 0f
                binder!!.playlistImage6.strokeWidth = 0f
                binder!!.playlistImage7.strokeWidth = 0f
                binder!!.playlistImage8.strokeWidth = 0f
                binder!!.playlistImage9.strokeWidth = 10f
                selectedImage = R.drawable.sad
            }
        }
    }
}