package com.AI.aimusic.view.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.AI.aimusic.Adapters.SongSelectionAdapter
import com.AI.aimusic.Interfaces.RecyclerVIewClickListener
import com.AI.aimusic.Interfaces.SongSelectionRecyclerVIewClickListener
import com.AI.aimusic.Model.MediaData
import com.AI.aimusic.R
import com.AI.aimusic.databinding.ActivitySongSelectionBinding
import com.AI.aimusic.databinding.SongSelectionBinding
import com.AI.aimusic.db.PlaylistSongEntity
import com.AI.aimusic.viewmodel.FavouriteMoviesViewModel
import java.util.*
import kotlin.collections.ArrayList

class SongSelectionActivity : AppCompatActivity(), View.OnClickListener, SongSelectionRecyclerVIewClickListener {

    private var binding:ActivitySongSelectionBinding ?= null
    private var adapter:SongSelectionAdapter ?= null
    private var selectionSongList = ArrayList<MediaData>()
    private var filteredList = ArrayList<MediaData>()
    private var playlistId:Int = 0;
    private lateinit var favouriteMoviesViewModel:FavouriteMoviesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySongSelectionBinding.inflate(layoutInflater)
        setContentView(binding!!.root)

        favouriteMoviesViewModel = ViewModelProvider(this).get(FavouriteMoviesViewModel::class.java)

        playlistId = intent.getIntExtra("playlistId", 0)

        initData()

    }

    private fun initData() {

        selectionSongList.addAll(SongsListActivity.mediaDataArrayList)

        binding!!.selectionRecyclerView.layoutManager =
            LinearLayoutManager(this)
        binding!!.selectionRecyclerView.isNestedScrollingEnabled = false
        binding!!.selectionRecyclerView.setHasFixedSize(true)
        binding!!.selectionRecyclerView.itemAnimator = DefaultItemAnimator()

        adapter = SongSelectionAdapter(selectionSongList, this, this)
        binding!!.selectionRecyclerView.adapter = adapter

        binding!!.clear.setOnClickListener(this)

        binding!!.search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                filteredList.clear()
                for (media in selectionSongList) {
                    if (media.title!!.lowercase(Locale.getDefault()).trim()
                            .contains(p0.toString().lowercase(Locale.getDefault()))
                    ) {

                        filteredList.add(media)
                    }
                }

                adapter!!.updateMusicList(filteredList)

                if (p0?.length!! > 0) {
                    showClearOption(true)
                }
                else {
                    showClearOption(false)
                }

            }

        })
    }

    private fun showClearOption(b: Boolean) {
        if (b) {
            if (binding!!.clear.visibility == View.GONE)
                binding!!.clear.visibility = View.VISIBLE
        }
        else {
            if (binding!!.clear.visibility == View.VISIBLE)
                binding!!.clear.visibility = View.GONE
        }
    }

    override fun onClick(p0: View?) {
        when(p0!!.id) {
            R.id.clear -> {
                binding!!.search.setText("")
            }
        }
    }

    override fun onSongSelectionItemClick(position: Int, binding: SongSelectionBinding) {
        val playlistSongEntity = PlaylistSongEntity(
            0,
            selectionSongList[position].title!!,
            selectionSongList[position].artist!!,
            selectionSongList[position].songPath!!,
            selectionSongList[position].album!!,
            playlistId
        )
        favouriteMoviesViewModel.insertSongToPlaylist(playlistSongEntity)
        Toast.makeText(this, "Added", Toast.LENGTH_SHORT).show()
        binding.playIcon.setImageResource(R.drawable.ic_check)

    }

}