package com.AI.aimusic.view.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Toast;

import com.AI.aimusic.Adapters.SongsAdapter;
import com.AI.aimusic.Interfaces.RecyclerVIewClickListener;
import com.AI.aimusic.Model.MediaData;
import com.AI.aimusic.R;
import com.AI.aimusic.Services.MusicService;
import com.AI.aimusic.UtilService.SharedPreferenceClass;
import com.AI.aimusic.databinding.ActivityMusicPlayerBinding;
import com.AI.aimusic.view.Fagments.NowPlaying;
import com.AI.aimusic.viewmodel.FavouriteMoviesViewModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSource;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.AI.aimusic.db.SongEntity;

import java.io.File;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Locale;

public class MusicPlayerActivity extends AppCompatActivity implements View.OnClickListener, ServiceConnection, RecyclerVIewClickListener {

    public static ActivityMusicPlayerBinding binding;
    private File songFile;
    private Handler handler;
    public static int position = 0;
    public static ArrayList<MediaData> mediaDataArrayList;
    public static MusicService musicService = null;
    public static String from = "";
    private BottomSheetBehavior<View> bottomSheetBehavior;
    private SongsAdapter songsAdapter;
    private FavouriteMoviesViewModel viewModel;
    private SharedPreferenceClass sharedPreferenceClass;

    Player.Listener playerListener = new Player.Listener() {
        @Override
        public void onPlaybackStateChanged(int playbackState) {
            switch (playbackState) {
                case ExoPlayer.STATE_ENDED:
                    //Stop playback and return to start position
                    prevNextSong(true);
                    NowPlaying.updateLayout();
                    break;
                case ExoPlayer.STATE_READY:
                    setProgress();
                    break;
                case ExoPlayer.STATE_BUFFERING:

                    break;
                case ExoPlayer.STATE_IDLE:

                    break;
            }
        }

        @Override
        public void onPlayWhenReadyChanged(boolean playWhenReady, int reason) {

        }

        @Override
        public void onIsPlayingChanged(boolean isPlaying) {


        }

        @Override
        public void onPlayerError(PlaybackException error) {
            Toast.makeText(MusicPlayerActivity.this, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
        }


        @Override
        public void onSeekForwardIncrementChanged(long seekForwardIncrementMs) {

        }

        @Override
        public void onSeekBackIncrementChanged(long seekBackIncrementMs) {

        }


    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMusicPlayerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.mainLayout.getForeground().setAlpha(0);

        mediaDataArrayList = getIntent().getExtras().getParcelableArrayList("Songs");
        position = getIntent().getIntExtra("position", 0);
        from = getIntent().getStringExtra("from");

        initValues();
    }

    private void initValues() {

        viewModel = new ViewModelProvider(this).get(FavouriteMoviesViewModel.class);

        sharedPreferenceClass = new SharedPreferenceClass(this);

        if (getIntent().getData() != null) {
            if (getIntent().getData().getScheme().contentEquals("content")) {
                playSongPickedFromFile(getIntent().getData());
            }
        } else {
            initializeList();
        }

        binding.title.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        binding.title.setSelected(true);

        bottomSheetBehavior = BottomSheetBehavior.from(binding.bottomSheet);
        bottomSheetBehavior.setPeekHeight(0);
        bottomSheetBehavior.setDraggable(true);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setMaxHeight(1200);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        binding.playButton.setOnClickListener(this);
        binding.backArrow.setOnClickListener(this);
        binding.progressSeek.setOnSeekBarChangeListener(seekListener);
        binding.previous.setOnClickListener(this);
        binding.next.setOnClickListener(this);
        binding.showBottomsheet.setOnClickListener(this);
        binding.favourite.setOnClickListener(this);
        binding.loop.setOnClickListener(this);
        binding.mainLayout.setOnClickListener(this);


        bottomSheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    binding.mainLayout.getForeground().setAlpha(0);
                }
                else {
                    binding.mainLayout.getForeground().setAlpha(190);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });

        setUpRecyclerViewForBottomSheet();

    }

    private void setUpRecyclerViewForBottomSheet() {
        binding.bottomRecyclerView.setLayoutManager(
                new LinearLayoutManager(this)
        );
        binding.bottomRecyclerView.setNestedScrollingEnabled(false);
        binding.bottomRecyclerView.setHasFixedSize(true);
        binding.bottomRecyclerView.setItemAnimator(new DefaultItemAnimator());
        songsAdapter = new SongsAdapter(mediaDataArrayList, this, "songsList", this);
        binding.bottomRecyclerView.setAdapter(songsAdapter);
        songsAdapter.notifyDataSetChanged();
    }

    private void playSongPickedFromFile(Uri data) {
//
//            mediaDataArrayList.add(getMediaObject(data));
        binding.title.setText(mediaDataArrayList.get(position).getTitle());
        binding.artist.setText(mediaDataArrayList.get(position).getArtist());

        Glide.with(getApplicationContext())
                .load(mediaDataArrayList.get(position).getAlbum())
                .apply(RequestOptions.placeholderOf(R.drawable.music1))
                .centerCrop()
                .into(binding.mainAlbum);
//            Intent intent = new Intent(this, MusicService.class);
//            bindService(intent, this, BIND_AUTO_CREATE);
//            startService(intent);
    }

//    private MediaData getMediaObject(Uri data) {
//
//
//    }

    private void initializeList() {

        if (from.equals("nowPlaying")) {
            binding.progressSeek.setOnSeekBarChangeListener(null);
            binding.progressSeek.setOnSeekBarChangeListener(seekListener);
            setProgress();
            if (musicService.exoPlayer.isPlaying()) {
                binding.playButton.setImageResource(R.drawable.ic_pause);
                binding.playButton.setTag("Pause");
            } else {
                binding.playButton.setImageResource(R.drawable.ic_play_white);
                binding.playButton.setTag("Play");
            }
        }
        else {
            Intent intent = new Intent(this, MusicService.class);
            bindService(intent, this, BIND_AUTO_CREATE);
            startService(intent);
        }

        setLayoutData();
    }

    private void setLayoutData() {
        binding.title.setText(mediaDataArrayList.get(position).getTitle());
        binding.artist.setText(mediaDataArrayList.get(position).getArtist());

        Glide.with(getApplicationContext())
                .load(mediaDataArrayList.get(position).getAlbum())
                .apply(RequestOptions.placeholderOf(R.drawable.music1))
                .centerCrop()
                .into(binding.mainAlbum);

        if (viewModel.isAddedToFavourite(mediaDataArrayList.get(position).getSongPath())) {
            binding.favourite.setImageResource(R.drawable.ic_favorite_fill);
            binding.favourite.setTag("Added");
        }
        else {
            binding.favourite.setImageResource(R.drawable.ic_favourite_outline);
            binding.favourite.setTag("Add");
        }

        if (sharedPreferenceClass.getValue_String(getString(R.string.loop_type)).
                equals("loop")) {
            binding.loop.setColorFilter(getResources().getColor(R.color.blue));
        }
        else {
            binding.loop.setColorFilter(getResources().getColor(R.color.icon_color));
        }

    }

    private void togglePlayPause() {

        if (binding.playButton.getTag().equals("Play")) {
            musicService.exoPlayer.play();
            binding.playButton.setImageResource(R.drawable.ic_pause);
            binding.playButton.setTag("Pause");
            musicService.updateActions(R.drawable.ic_pause, "playPause");
        } else {
            musicService.exoPlayer.pause();
            binding.playButton.setImageResource(R.drawable.ic_play_white);
            binding.playButton.setTag("Play");
            musicService.updateActions(R.drawable.ic_play, "playPause");
        }
    }

    private void setUpPlayer() {
        String userAgent = "";

        if (musicService.exoPlayer == null) {
            DefaultRenderersFactory renderersFactory = new DefaultRenderersFactory(this).setExtensionRendererMode(DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF);
            TrackSelector trackSelector = new DefaultTrackSelector(this);
            musicService.exoPlayer = new ExoPlayer.Builder(this, renderersFactory).setTrackSelector(trackSelector).build();
            musicService.exoPlayer.addListener(playerListener);
            userAgent = Util.getUserAgent(this, "Play Audio");
        }
        songFile = new File(mediaDataArrayList.get(position).getSongPath());
        if (sharedPreferenceClass.getValue_String(getString(R.string.loop_type)).
                equals("loop")) {
            musicService.exoPlayer.setRepeatMode(Player.REPEAT_MODE_ALL);
        }
        else {
            musicService.exoPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);
        }
        DefaultDataSource.Factory factory = new DefaultDataSource.Factory(this);
        Uri uri = Uri.fromFile(songFile);
        MediaItem mediaItem = new MediaItem.Builder().setUri(uri).build();
        ProgressiveMediaSource mediaSource = new ProgressiveMediaSource.Factory(factory).setDrmUserAgent(userAgent).createMediaSource(mediaItem);
        musicService.exoPlayer.setMediaSource(mediaSource);
        musicService.exoPlayer.prepare();
        musicService.exoPlayer.setPlayWhenReady(true);
        setProgress();
        binding.playButton.setImageResource(R.drawable.ic_pause);
        binding.playButton.setTag("Pause");
        musicService.showNotification(R.drawable.ic_pause);
    }

    private void setProgress() {

        binding.progressSeek.setProgress((int) musicService.exoPlayer.getCurrentPosition() / 1000);

        binding.progressSeek.setMax((int) musicService.exoPlayer.getDuration() / 1000);
        binding.currentSeektime.setText(stringForTime((int) musicService.exoPlayer.getCurrentPosition()));
        binding.totalSeektime.setText(stringForTime((int) musicService.exoPlayer.getDuration()));

        if (handler == null) handler = new Handler();
        //Make sure you update Seekbar on UI thread
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (musicService.exoPlayer != null && musicService.exoPlayer.isPlaying()) {
                    binding.progressSeek.setMax((int) musicService.exoPlayer.getDuration() / 1000);
                    int mCurrentPosition = (int) musicService.exoPlayer.getCurrentPosition() / 1000;
                    binding.progressSeek.setProgress(mCurrentPosition);
                    binding.currentSeektime.setText(stringForTime((int) musicService.exoPlayer.getCurrentPosition()));
                    binding.totalSeektime.setText(stringForTime((int) musicService.exoPlayer.getDuration()));

                    handler.postDelayed(this, 1000);
                }
            }
        });
    }


    private String stringForTime(int timeMs) {
        StringBuilder mFormatBuilder;
        Formatter mFormatter;
        mFormatBuilder = new StringBuilder();
        mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());
        int totalSeconds = timeMs / 1000;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;

        mFormatBuilder.setLength(0);
        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }

    private void prevNextSong(boolean increment) {
        musicService.setSongPosition(increment);
        setLayoutData();
        setUpPlayer();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_arrow:
                onBackPressed();
                break;
            case R.id.play_button:
                togglePlayPause();
                break;
            case R.id.previous:
                prevNextSong(false);
                break;
            case R.id.next:
                prevNextSong(true);
                break;
            case R.id.loop:
                if (sharedPreferenceClass.getValue_String(getString(R.string.loop_type)).
                        equals("no_loop")) {
                    sharedPreferenceClass.setValue_String(getString(R.string.loop_type), "loop");
                    binding.loop.setColorFilter(getResources().getColor(R.color.blue));
                    musicService.exoPlayer.setRepeatMode(Player.REPEAT_MODE_ALL);
                    Toast.makeText(this, "Loop Song", Toast.LENGTH_SHORT).show();
                }
                else {
                    sharedPreferenceClass.setValue_String(getString(R.string.loop_type), "no_loop");
                    binding.loop.setColorFilter(getResources().getColor(R.color.icon_color));
                    musicService.exoPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);
                    Toast.makeText(this, "Loop List", Toast.LENGTH_SHORT).show();
                }
                    break;
            case R.id.favourite:
                if (binding.favourite.getTag().equals("Added")) {
                    viewModel.deleteFromFavourite(mediaDataArrayList.get(position).getSongPath());
                    binding.favourite.setImageResource(R.drawable.ic_favourite_outline);
                    binding.favourite.setTag("Add");
                    Toast.makeText(this, "Removed from Favourite", Toast.LENGTH_SHORT).show();
                }
                else {
                    SongEntity songEntity = new SongEntity(0,
                            mediaDataArrayList.get(position).getTitle(),
                            mediaDataArrayList.get(position).getArtist(),
                            mediaDataArrayList.get(position).getSongPath(),
                            mediaDataArrayList.get(position).getAlbum());
                    viewModel.insertFavouriteMovies(songEntity);
                    binding.favourite.setImageResource(R.drawable.ic_favorite_fill);
                    binding.favourite.setTag("Added");
                    Toast.makeText(this, "Added to Favourite", Toast.LENGTH_SHORT).show();

                }
                break;
            case R.id.show_bottomsheet:
                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    binding.bottomRecyclerView.scrollToPosition(position);
                } else {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
                break;

            case R.id.main_layout:
                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;

        }

    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        MusicService.MyBinder binder = (MusicService.MyBinder) iBinder;
        musicService = binder.currentService();
        setUpPlayer();
        if (musicService.audioManager == null) {
            musicService.audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            musicService.audioManager.requestAudioFocus(musicService,
                    AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
//            musicService.audioManager.setParameters("noise_suppression=on");
//            musicService.audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
//            musicService.audioManager.setSpeakerphoneOn(true);
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        if (MusicPlayerActivity.musicService.speechService != null) {
            MusicPlayerActivity.musicService.speechService.stop();
            MusicPlayerActivity.musicService.speechService.shutdown();
        }
        musicService = null;

    }

    private final SeekBar.OnSeekBarChangeListener seekListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            if (!b) {
                return;
            }

            musicService.exoPlayer.seekTo(i * 1000L);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }

    };

    @Override
    public void onBackPressed() {
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.stay, R.anim.slide_down);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaDataArrayList.get(position).getAlbum().equals("Unknown")
                && !musicService.exoPlayer.isPlaying()) {
            musicService.stopForeground(true);
            musicService.exoPlayer.release();
            if (musicService.audioManager != null)
                musicService.audioManager.abandonAudioFocus(MusicPlayerActivity.musicService);
            if (MusicPlayerActivity.musicService.speechService != null) {
                MusicPlayerActivity.musicService.speechService.stop();
                MusicPlayerActivity.musicService.speechService.shutdown();
            }
            musicService = null;
        }

    }


    @Override
    public void onItemClick(int position1, @NonNull String from1) {
        if (!from1.equals("nowPlaying")) {
            position = position1;
            setUpPlayer();
            setLayoutData();
        }
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    @Override
    public void onDeleteClick(int position) {

    }
}
