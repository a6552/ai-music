package com.AI.aimusic.view.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.AI.aimusic.Adapters.SongsAdapter
import com.AI.aimusic.Interfaces.RecyclerVIewClickListener
import com.AI.aimusic.Model.MediaData
import com.AI.aimusic.R
import com.AI.aimusic.databinding.ActivityFavouriteBinding
import com.AI.aimusic.viewmodel.FavouriteMoviesViewModel

class FavouriteActivity : AppCompatActivity(), RecyclerVIewClickListener {

    private var binding: ActivityFavouriteBinding? = null
    private var songsAdapter: SongsAdapter? = null
    private lateinit var favouriteMoviesViewModel: FavouriteMoviesViewModel
    private var mediaDataArrayList = ArrayList<MediaData>()
    private var secondArrayList = ArrayList<MediaData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFavouriteBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
        initData()

    }

    private fun initData() {

        favouriteMoviesViewModel = ViewModelProvider(this).get(FavouriteMoviesViewModel::class.java)

        binding!!.favouriteRecyclerView.layoutManager =
            LinearLayoutManager(this)
        binding!!.favouriteRecyclerView.isNestedScrollingEnabled = false
        binding!!.favouriteRecyclerView.setHasFixedSize(true)
        binding!!.favouriteRecyclerView.itemAnimator = DefaultItemAnimator()

        favouriteMoviesViewModel.getAllSongsInFavourite().observe(this, Observer {
            if (it != null) {
                mediaDataArrayList.clear()
                secondArrayList.clear()
                if (it.isEmpty()) {
                    if ( binding!!.emptyFavourite.visibility == GONE) {
                        binding!!.emptyFavourite.visibility = VISIBLE
                        binding!!.noFavTxt.visibility = VISIBLE
                    }
                }
                else {
                    if ( binding!!.emptyFavourite.visibility == VISIBLE) {
                        binding!!.emptyFavourite.visibility = GONE
                        binding!!.noFavTxt.visibility = GONE
                    }
                    for (entity in it) {
                        mediaDataArrayList.add(MediaData(entity.title, entity.artist, entity.songPath, entity.album))
                    }
                    songsAdapter = SongsAdapter(mediaDataArrayList, this, getString(R.string.favouriteList), this)
                    binding!!.favouriteRecyclerView.adapter = songsAdapter
                    songsAdapter!!.notifyDataSetChanged()
                }
            }
        })


    }

    override fun onItemClick(position: Int, from: String) {
        val intent = Intent(this, MusicPlayerActivity::class.java)
        val bundle = Bundle()
        bundle.putParcelableArrayList("Songs", mediaDataArrayList)
        intent.putExtras(bundle)
        intent.putExtra("from", from)
        intent.putExtra("position", position)
        startActivity(intent)
    }

    override fun onDeleteClick(position: Int) {
        TODO("Not yet implemented")
    }
}