package com.AI.aimusic.view.Activity

import android.Manifest
import android.animation.Animator
import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import com.AI.aimusic.Model.MediaData
import com.AI.aimusic.Adapters.SongsAdapter
import android.os.Bundle
import com.karumi.dexter.Dexter
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.MultiplePermissionsReport
import android.content.Intent
import android.graphics.Color
import com.karumi.dexter.PermissionToken
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.AI.aimusic.databinding.ActivitySongsListBinding
import com.karumi.dexter.listener.PermissionRequest
import java.util.*
import kotlin.collections.ArrayList
import kotlin.system.exitProcess
import android.view.MotionEvent
import android.view.View
import android.view.View.*
import android.view.ViewTreeObserver
import android.view.animation.AnimationUtils

import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.AI.aimusic.Interfaces.RecyclerVIewClickListener

import com.AI.aimusic.R
import android.view.animation.TranslateAnimation
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.AI.aimusic.viewmodel.SongsListViewModel


class SongsListActivity : AppCompatActivity(), OnClickListener, RecyclerVIewClickListener {
    private var binding: ActivitySongsListBinding? = null
    private var songsAdapter: SongsAdapter? = null
    private var filterDataArrayList = ArrayList<MediaData>()
    private lateinit var songsListViewModel:SongsListViewModel

    companion object {
        @JvmField
        var mediaDataArrayList = ArrayList<MediaData>()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySongsListBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
        songsListViewModel = ViewModelProvider(this).get(SongsListViewModel::class.java)
        checkVoiceCommandPermission()
        initData()
    }


    private fun initData() {
        binding!!.songsRecyclerView.layoutManager =
            LinearLayoutManager(this)
        binding!!.songsRecyclerView.isNestedScrollingEnabled = false
        binding!!.songsRecyclerView.setHasFixedSize(true)
        binding!!.songsRecyclerView.itemAnimator = DefaultItemAnimator()
        binding!!.clear.setOnClickListener(this)


        binding!!.search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                filterDataArrayList.clear()
                    for (media in mediaDataArrayList) {
                        if (media.title!!.lowercase(Locale.getDefault()).trim()
                                .contains(p0.toString().lowercase(Locale.getDefault()))
                        ) {

                            filterDataArrayList.add(media)
                        }
                    }

                    songsAdapter!!.updateMusicList(filterDataArrayList)

                if (p0?.length!! > 0) {
                    showClearOption(true)
                }
                else {
                    showClearOption(false)
                }

            }

        })

        binding!!.songsRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (recyclerView.scrollState == RecyclerView.SCROLL_STATE_SETTLING)
                    if (dy > 0) {
                        binding!!.topLayout.visibility = GONE
                    } else if (dy < 0) {
                        binding!!.topLayout.visibility = VISIBLE

                    }

            }
        });

        binding!!.favouriteBox.setOnClickListener(this)
        binding!!.playlistBox.setOnClickListener(this)
        binding!!.voiceBox.setOnClickListener(this)
        binding!!.trendingBox.setOnClickListener(this)

    }

    private fun showClearOption(b: Boolean) {
        if (b) {
            if (binding!!.clear.visibility == GONE)
                binding!!.clear.visibility = VISIBLE
        }
        else {
            if (binding!!.clear.visibility == VISIBLE)
                binding!!.clear.visibility = GONE
        }
    }

    private fun checkVoiceCommandPermission() {
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(multiplePermissionsReport: MultiplePermissionsReport) {
                    if (multiplePermissionsReport.areAllPermissionsGranted()) {
                        mediaDataArrayList.clear()

                        songsListViewModel.getAllSongs(this@SongsListActivity)
                            .observe(this@SongsListActivity, androidx.lifecycle.Observer {
                                if (it != null) {
                                    mediaDataArrayList = it as ArrayList<MediaData>
                                    songsAdapter = SongsAdapter(mediaDataArrayList, this@SongsListActivity, getString(R.string.from_songsList), this@SongsListActivity)
                                    binding!!.songsRecyclerView.adapter = songsAdapter
                                    songsAdapter!!.notifyDataSetChanged()
                                }

                            })

                    } else {
                        AlertDialog.Builder(this@SongsListActivity)
                            .setTitle("Permission Required")
                            .setMessage(
                                "These permissions are required to run this app. " +
                                        "Please Allow required permissions to use this app."
                            ) // Specifying a listener allows you to take an action before dismissing the dialog.
                            // The dialog is automatically dismissed when a dialog button is clicked.
                            .setPositiveButton("Allow") { dialog, which ->
                                val intent = Intent()
                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                val uri = Uri.fromParts("package", packageName, null)
                                intent.data = uri
                                startActivity(intent)
                            }
                            .show()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    list: List<PermissionRequest>,
                    permissionToken: PermissionToken
                ) {
                    permissionToken.continuePermissionRequest()
                }
            }).check()
    }


    override fun onDestroy() {
        super.onDestroy()
        if (MusicPlayerActivity.musicService != null) {
            if (!MusicPlayerActivity.musicService.exoPlayer.isPlaying) {
                MusicPlayerActivity.musicService.stopForeground(true)
                MusicPlayerActivity.musicService.stopSelf()
                MusicPlayerActivity.musicService.exoPlayer.release()
                MusicPlayerActivity.musicService.audioManager.abandonAudioFocus(MusicPlayerActivity.musicService)
                if (MusicPlayerActivity.musicService.speechService != null) {
                    MusicPlayerActivity.musicService.speechService.stop()
                    MusicPlayerActivity.musicService.speechService.shutdown()
                }
                MusicPlayerActivity.musicService = null

            }
        }
    }

    override fun onClick(p0: View?) {
        when(p0!!.id) {
            R.id.clear -> {
                binding!!.search.setText("")
            }

            R.id.favourite_box -> {
                val intent = Intent(this, FavouriteActivity::class.java)
                startActivity(intent)
            }

            R.id.playlist_box -> {
                val intent = Intent(this, PlaylistActivity::class.java)
                startActivity(intent)
            }

            R.id.voice_box -> {
                val intent = Intent(this, VoiceCommandActivity::class.java)
                startActivity(intent)
            }

            R.id.trending_box -> {
                val intent = Intent(this, TrendingSongsActivity::class.java)
                startActivity(intent)
            }
        }
    }

    override fun onItemClick(position: Int, from: String) {
        val intent = Intent(this, MusicPlayerActivity::class.java)
        val bundle = Bundle()
        bundle.putParcelableArrayList("Songs", mediaDataArrayList)
        intent.putExtras(bundle)
        intent.putExtra("from", from)
        intent.putExtra("position", position)
        startActivity(intent)
    }

    override fun onDeleteClick(position: Int) {
        TODO("Not yet implemented")
    }


}