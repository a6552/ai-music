package com.AI.aimusic.view.Fagments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.AI.aimusic.R
import com.AI.aimusic.databinding.PlayingNowBinding
import com.AI.aimusic.view.Activity.MusicPlayerActivity
import com.AI.aimusic.view.Activity.MusicPlayerActivity.from
import com.AI.aimusic.view.Activity.MusicPlayerActivity.position
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSource
import java.io.File

class NowPlaying : Fragment(), View.OnClickListener {

    private lateinit var mainView:View

    companion object {
        lateinit var binding:PlayingNowBinding
        private lateinit var context: Context

        fun setContext(con: Context) {
            context=con
        }

        @JvmStatic
        fun removeNowPlaying() {
            binding.root.visibility = View.INVISIBLE
        }

        @JvmStatic
        fun updateLayout() {
            binding.titleNP.text =
                MusicPlayerActivity.mediaDataArrayList[MusicPlayerActivity.position].title
            binding.artistNP.text =
                MusicPlayerActivity.mediaDataArrayList[MusicPlayerActivity.position].artist

            Glide.with(context)
                .load(MusicPlayerActivity.mediaDataArrayList[MusicPlayerActivity.position].album)
                .apply(RequestOptions.placeholderOf(R.drawable.music1))
                .centerCrop()
                .into(binding.album)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mainView = inflater.inflate(R.layout.playing_now, container, false)
        binding = PlayingNowBinding.bind(mainView)
        setContext(requireContext())
        binding.titleNP.ellipsize = TextUtils.TruncateAt.MARQUEE
        binding.titleNP.isSelected = true
        binding.root.visibility = View.INVISIBLE
        binding.playButtonNP.setOnClickListener(this)
        binding.nextButtonNP.setOnClickListener(this)
        binding.root.setOnClickListener(this);

        return mainView
    }

    override fun onResume() {
        super.onResume()
        if (MusicPlayerActivity.musicService != null) {
            binding.root.visibility = View.VISIBLE
            binding.titleNP.text =
                MusicPlayerActivity.mediaDataArrayList[position].title
            binding.artistNP.text =
                MusicPlayerActivity.mediaDataArrayList[position].artist

            Glide.with(requireContext())
                .load(MusicPlayerActivity.mediaDataArrayList[position].album)
                .apply(RequestOptions.placeholderOf(R.drawable.music1))
                .centerCrop()
                .into(binding.album)

            setPlayPause()
        }

    }

    private fun setPlayPause(){
        if (MusicPlayerActivity.musicService.exoPlayer.isPlaying) {
            binding.playButtonNP.setImageResource(R.drawable.ic_pause)
            binding.playButtonNP.tag = "Pause"
        }
        else {
            binding.playButtonNP.setImageResource(R.drawable.ic_play)
            binding.playButtonNP.tag = "Play"
        }
    }

    private fun togglePlayPause() {
        if (binding.playButtonNP.tag.equals("Play")) {
            MusicPlayerActivity.musicService.exoPlayer.play()
            MusicPlayerActivity.musicService.updateActions(R.drawable.ic_pause, "playPause")
            binding.playButtonNP.setImageResource(R.drawable.ic_pause)
            binding.playButtonNP.tag = "Pause"
        } else {
            MusicPlayerActivity.musicService.exoPlayer.pause()
            MusicPlayerActivity.musicService.updateActions(R.drawable.ic_play, "playPause")
            binding.playButtonNP.setImageResource(R.drawable.ic_play)
            binding.playButtonNP.tag = "Play"
        }
    }

    override fun onClick(p0: View?) {
        when(p0!!.id) {
            R.id.play_buttonNP -> {
                togglePlayPause()
            }
            R.id.next_buttonNP -> {
                prevNextSong(true)
            }
            R.id.root -> {
                val intent = Intent(requireContext(), MusicPlayerActivity::class.java)
                val bundle = Bundle()
                bundle.putParcelableArrayList("Songs", MusicPlayerActivity.mediaDataArrayList)
                intent.putExtras(bundle)
                intent.putExtra("from", "nowPlaying")
                intent.putExtra("position", position)
                startActivity(intent)
                requireActivity().overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        }
    }

    private fun prevNextSong(increment: Boolean) {
        MusicPlayerActivity.musicService.setSongPosition(increment)
        var songFile =
            File(MusicPlayerActivity.mediaDataArrayList[MusicPlayerActivity.position].songPath)

        val factory = DefaultDataSource.Factory(requireContext())
        val uri = Uri.fromFile(songFile)
        val mediaItem = MediaItem.Builder().setUri(uri).build()
        val mediaSource = ProgressiveMediaSource.Factory(factory)
            .createMediaSource(mediaItem)
        MusicPlayerActivity.musicService.exoPlayer.setMediaSource(mediaSource)
        MusicPlayerActivity.musicService.exoPlayer.prepare()

      binding.titleNP.text =
            MusicPlayerActivity.mediaDataArrayList[MusicPlayerActivity.position].title
        binding.artistNP.text =
            MusicPlayerActivity.mediaDataArrayList[MusicPlayerActivity.position].artist
        Glide.with(requireContext())
            .load(MusicPlayerActivity.mediaDataArrayList[MusicPlayerActivity.position].album)
            .apply(RequestOptions.placeholderOf(R.drawable.music1))
            .centerCrop()
            .into(binding.album)
        MusicPlayerActivity.musicService.updateActions(R.drawable.ic_pause, "next")
        MusicPlayerActivity.musicService!!.exoPlayer.playWhenReady = true
        binding.playButtonNP.setImageResource(R.drawable.ic_pause)
    }



}