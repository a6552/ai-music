package com.AI.aimusic.view.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.animation.TranslateAnimation
import com.AI.aimusic.R
import com.AI.aimusic.databinding.ActivitySplashBinding
import com.bumptech.glide.Glide
import android.view.animation.AlphaAnimation

import android.view.animation.Animation




class SplashActivity : AppCompatActivity() {

    private var binding:ActivitySplashBinding?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding!!.root)

        var animation = TranslateAnimation(0.0f, -220.0f,
            0.0f, 0.0f)
        animation.duration = 800
        animation.fillAfter = true

        val anim2 = AlphaAnimation(0.0f, 1.0f)
        anim2.duration = 1000

        Handler(Looper.getMainLooper()).postDelayed(
            {
                binding!!.music.startAnimation(animation)
            },
            500)

        Handler(Looper.getMainLooper()).postDelayed(
            {
                binding!!.logoTxt.visibility = View.VISIBLE
                binding!!.logoTxt.startAnimation(anim2)
            },
            900)

        Handler(Looper.getMainLooper()).postDelayed(
            {
                val intent = Intent(this, SongsListActivity::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                finish()

            },
            2100)

    }

}