package com.AI.aimusic.view.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.AI.aimusic.Adapters.SongsAdapter
import com.AI.aimusic.Interfaces.RecyclerVIewClickListener
import com.AI.aimusic.Model.MediaData
import com.AI.aimusic.R
import com.AI.aimusic.databinding.ActivityPlaylistDetailsBinding
import com.AI.aimusic.viewmodel.FavouriteMoviesViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions


class PlaylistDetailsActivity : AppCompatActivity(), View.OnClickListener, RecyclerVIewClickListener {

    private var binding:ActivityPlaylistDetailsBinding ?= null
    private var adapter:SongsAdapter ?= null
    private lateinit var favouriteMoviesViewModel: FavouriteMoviesViewModel
    private var songsList = ArrayList<MediaData>()
    private var playListId: Int = -1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlaylistDetailsBinding.inflate(layoutInflater)
        setContentView(binding!!.root)

        playListId = intent.getIntExtra("playlistId", 0)
        binding!!.playlistName.text = intent.getStringExtra("playlistName")

        favouriteMoviesViewModel = ViewModelProvider(this).get(FavouriteMoviesViewModel::class.java)
        initData()

    }

    private fun initData() {
        var imageId = intent.getIntExtra("image", 0)
        binding!!.playlistImage.setImageResource(imageId)
        binding!!.songsRecyclerView.layoutManager =
            LinearLayoutManager(this)
        binding!!.songsRecyclerView.isNestedScrollingEnabled = false
        binding!!.songsRecyclerView.setHasFixedSize(true)
        binding!!.songsRecyclerView.itemAnimator = DefaultItemAnimator()
        adapter = SongsAdapter(songsList,
            this, getString(R.string.from_playlist), this)
        binding!!.songsRecyclerView.adapter = adapter

        binding!!.addSongs.setOnClickListener(this)
        binding!!.removeAllSongs.setOnClickListener(this)
        binding!!.backArrow.setOnClickListener(this)
        binding!!.menu.setOnClickListener(this)

        getAllSongs()
    }

    private fun getAllSongs() {
        favouriteMoviesViewModel.getAllPlaylistSongs(playListId).observe(this, Observer {
            if (it != null) {
                binding!!.totalSongs.text = "Total Songs \u2022 "+it.size.toString()
                songsList.clear()
                for (media in it) {
                    songsList.add(MediaData(media.title, media.artist, media.songPath, media.album))
                }
                adapter!!.notifyDataSetChanged()
            }
        })

        if (adapter!!.itemCount > 0) {
            Glide.with(this)
                .load(
                    songsList[0].album
                )
                .apply(RequestOptions.placeholderOf(R.drawable.party))
                .into(binding!!.playlistImage)
        }
    }

    override fun onClick(p0: View?) {
        when(p0!!.id) {
            R.id.back_arrow -> {
                onBackPressed()
            }
            R.id.add_songs -> {
                var intent = Intent(this, SongSelectionActivity::class.java)
                intent.putExtra("playlistId", playListId)
                startActivity(intent)
            }
            R.id.remove_all_songs -> {
                favouriteMoviesViewModel.deleteSongsFromPlaylist(playListId)
                Toast.makeText(this, "Deleted", Toast.LENGTH_SHORT).show()
            }
            R.id.menu -> {
                val popupMenu = PopupMenu(this, binding!!.menu)
                popupMenu.inflate(R.menu.top_pop_up_menu)
                popupMenu.setOnMenuItemClickListener(object : MenuItem.OnMenuItemClickListener,
                    PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(p0: MenuItem?): Boolean {
                        when(p0!!.itemId) {
                            R.id.delete -> {
                                favouriteMoviesViewModel.deletePlaylist(playListId)
                                Toast.makeText(this@PlaylistDetailsActivity, "Deleted", Toast.LENGTH_SHORT).show()
                                val intent = Intent(this@PlaylistDetailsActivity, PlaylistActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                startActivity(intent)
                            }

                            R.id.add_songs -> {
                                var intent = Intent(this@PlaylistDetailsActivity, SongSelectionActivity::class.java)
                                intent.putExtra("playlistId", playListId)
                                startActivity(intent)
                            }
                        }
                        return true
                    }

                })
                popupMenu.show()

            }
        }
    }

    override fun onItemClick(position: Int, from: String) {
        val intent = Intent(this, MusicPlayerActivity::class.java)
        val bundle = Bundle()
        bundle.putParcelableArrayList("Songs", songsList)
        intent.putExtras(bundle)
        intent.putExtra("from", from)
        intent.putExtra("position", position)
        startActivity(intent)
    }

    override fun onDeleteClick(position: Int) {
        favouriteMoviesViewModel.deleteSingleSongsFromPlaylist(songsList[position].songPath!!, playListId)
        Toast.makeText(this, "Deleted", Toast.LENGTH_SHORT).show()
    }
}