package com.AI.aimusic.Interfaces

import android.os.Binder
import com.AI.aimusic.databinding.SongSelectionBinding

interface SongSelectionRecyclerVIewClickListener {

    fun onSongSelectionItemClick(position: Int, binding: SongSelectionBinding)
}