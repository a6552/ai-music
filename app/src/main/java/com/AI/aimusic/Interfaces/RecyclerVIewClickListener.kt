package com.AI.aimusic.Interfaces

import android.os.Binder
import com.AI.aimusic.databinding.SongSelectionBinding

interface RecyclerVIewClickListener {

    fun onItemClick(position: Int, from:String)
    fun onDeleteClick(position: Int)

}